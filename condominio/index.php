<?php

error_reporting(0);

$torreA = array(
    "Andar 1" => array(
        "Apto 101" => array("inativo"),
        "Apto 102" => array("Nome do Morador"),
        "Apto 103" => array("Nome do Morador"),
        "Apto 104" => array("Nome do Morador")
    ),
    "Andar 2" => array(
        "Apto 201" => array("Nome do Morador"),
        "Apto 202" => array("Nome do Morador"),
        "Apto 203" => array("Nome do Morador"),
        "Apto 204" => array("Nome do Morador")
    ),
    "Andar 3" => array(
        "Apto 301" => array("Nome do Morador"),
        "Apto 302" => array("inativo"),
        "Apto 303" => array("Nome do Morador"),
        "Apto 304" => array("Nome do Morador")
    ),
    "Andar 4" => array(
        "Apto 401" => array("inativo"),
        "Apto 402" => array("Nome do Morador"),
        "Apto 403" => array("Nome do Morador"),
        "Apto 404" => array("Nome do Morador")
    ),
    "Andar 5" => array(
        "Apto 501" => array("Nome do Morador"),
        "Apto 502" => array("Nome do Morador"),
        "Apto 503" => array("Nome do Morador"),
        "Apto 504" => array("Nome do Morador")
    ),
    "Andar 6" => array(
        "Apto 601" => array("Nome do Morador"),
        "Apto 602" => array("Nome do Morador"),
        "Apto 603" => array("Nome do Morador"),
        "Apto 604" => array("Nome do Morador")
    ),
    "Andar 7" => array(
        "Apto 701" => array("Nome do Morador"),
        "Apto 702" => array("Nome do Morador"),
        "Apto 703" => array("Nome do Morador"),
        "Apto 704" => array("Nome do Morador")
    ),
    "Andar 8" => array(
        "Apto 801" => array("Nome do Morador"),
        "Apto 802" => array("Nome do Morador"),
        "Apto 803" => array("Nome do Morador"),
        "Apto 804" => array("Nome do Morador")
    ),
);

$torreB = array(
    "Andar 1" => array(
        "Apto 101" => array("Nome do Morador"),
        "Apto 102" => array("Nome do Morador"),
        "Apto 103" => array("Nome do Morador"),
        "Apto 104" => array("Nome do Morador")
    ),
    "Andar 2" => array(
        "Apto 201" => array("Nome do Morador"),
        "Apto 202" => array("Nome do Morador"),
        "Apto 203" => array("Nome do Morador"),
        "Apto 204" => array("Nome do Morador")
    ),
    "Andar 3" => array(
        "Apto 301" => array("Nome do Morador"),
        "Apto 302" => array("Nome do Morador"),
        "Apto 303" => array("Nome do Morador"),
        "Apto 304" => array("Nome do Morador")
    ),
    "Andar 4" => array(
        "Apto 401" => array("Nome do Morador"),
        "Apto 402" => array("Nome do Morador"),
        "Apto 403" => array("Nome do Morador"),
        "Apto 404" => array("Nome do Morador")
    ),
    "Andar 5" => array(
        "Apto 501" => array("Nome do Morador"),
        "Apto 502" => array("Nome do Morador"),
        "Apto 503" => array("Nome do Morador"),
        "Apto 504" => array("Nome do Morador")
    ),
    "Andar 6" => array(
        "Apto 601" => array("Nome do Morador"),
        "Apto 602" => array("Nome do Morador"),
        "Apto 603" => array("Nome do Morador"),
        "Apto 604" => array("Nome do Morador")
    ),
    "Andar 7" => array(
        "Apto 701" => array("Nome do Morador"),
        "Apto 702" => array("Nome do Morador"),
        "Apto 703" => array("Nome do Morador"),
        "Apto 704" => array("Nome do Morador")
    ),
    "Andar 8" => array(
        "Apto 801" => array("Nome do Morador"),
        "Apto 802" => array("Nome do Morador"),
        "Apto 803" => array("Nome do Morador"),
        "Apto 804" => array("Nome do Morador")
    ),
);

$torreC = array(
    "Andar 1" => array(
        "Apto 101" => array("Nome do Morador"),
        "Apto 102" => array("Nome do Morador"),
        "Apto 103" => array("Nome do Morador"),
        "Apto 104" => array("Nome do Morador")
    ),
    "Andar 2" => array(
        "Apto 201" => array("Nome do Morador"),
        "Apto 202" => array("Nome do Morador"),
        "Apto 203" => array("Nome do Morador"),
        "Apto 204" => array("Nome do Morador")
    ),
    "Andar 3" => array(
        "Apto 301" => array("Nome do Morador"),
        "Apto 302" => array("Nome do Morador"),
        "Apto 303" => array("Nome do Morador"),
        "Apto 304" => array("Nome do Morador")
    ),
    "Andar 4" => array(
        "Apto 401" => array("Nome do Morador"),
        "Apto 402" => array("Nome do Morador"),
        "Apto 403" => array("Nome do Morador"),
        "Apto 404" => array("Nome do Morador")
    ),
    "Andar 5" => array(
        "Apto 501" => array("Nome do Morador"),
        "Apto 502" => array("Nome do Morador"),
        "Apto 503" => array("Nome do Morador"),
        "Apto 504" => array("Nome do Morador")
    ),
    "Andar 6" => array(
        "Apto 601" => array("Nome do Morador"),
        "Apto 602" => array("Nome do Morador"),
        "Apto 603" => array("Nome do Morador"),
        "Apto 604" => array("Nome do Morador")
    ),
    "Andar 7" => array(
        "Apto 701" => array("Nome do Morador"),
        "Apto 702" => array("Nome do Morador"),
        "Apto 703" => array("Nome do Morador"),
        "Apto 704" => array("Nome do Morador")
    ),
    "Andar 8" => array(
        "Apto 801" => array("Nome do Morador"),
        "Apto 802" => array("Nome do Morador"),
        "Apto 803" => array("Nome do Morador"),
        "Apto 804" => array("Nome do Morador")
    ),
);

$torreD = array(
    "Andar 1" => array(
        "Apto 101" => array("Nome do Morador"),
        "Apto 102" => array("Nome do Morador"),
        "Apto 103" => array("Nome do Morador"),
        "Apto 104" => array("Nome do Morador")
    ),
    "Andar 2" => array(
        "Apto 201" => array("Nome do Morador"),
        "Apto 202" => array("Nome do Morador"),
        "Apto 203" => array("Nome do Morador"),
        "Apto 204" => array("Nome do Morador")
    ),
    "Andar 3" => array(
        "Apto 301" => array("Nome do Morador"),
        "Apto 302" => array("Nome do Morador"),
        "Apto 303" => array("Nome do Morador"),
        "Apto 304" => array("Nome do Morador")
    ),
    "Andar 4" => array(
        "Apto 401" => array("Nome do Morador"),
        "Apto 402" => array("Nome do Morador"),
        "Apto 403" => array("Nome do Morador"),
        "Apto 404" => array("Nome do Morador")
    ),
    "Andar 5" => array(
        "Apto 501" => array("Nome do Morador"),
        "Apto 502" => array("Nome do Morador"),
        "Apto 503" => array("Nome do Morador"),
        "Apto 504" => array("Nome do Morador")
    ),
    "Andar 6" => array(
        "Apto 601" => array("Nome do Morador"),
        "Apto 602" => array("Nome do Morador"),
        "Apto 603" => array("Nome do Morador"),
        "Apto 604" => array("Nome do Morador")
    ),
    "Andar 7" => array(
        "Apto 701" => array("Nome do Morador"),
        "Apto 702" => array("Nome do Morador"),
        "Apto 703" => array("Nome do Morador"),
        "Apto 704" => array("Nome do Morador")
    ),
    "Andar 8" => array(
        "Apto 801" => array("Nome do Morador"),
        "Apto 802" => array("Nome do Morador"),
        "Apto 803" => array("Nome do Morador"),
        "Apto 804" => array("Nome do Morador")
    ),
);

$condominio = array(
    "Torre A" => $torreA,
    "Torre B" => $torreB,
    "Torre C" => $torreC,
    "Torre D" => $torreD
);

function verificaUnidade($morador) {
    if ($morador == "inativo") {
        return "<b>Unidade Vazia</b>";
    } else {
        return $morador;
    }
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Condomínio</title>
</head>
<body>
    <ul>
        <? foreach ($condominio as $chaveTorres => $torres) { ?>
            <li><?= $chaveTorres ?></li>
            <ul>
                <? foreach ($torres as $chaveAndares => $andares) { ?>
                    <li><?=$chaveAndares?></li>
                    <ul>
                        <? foreach ($andares as $chaveUnidades => $aptos) { ?>
                            <li><?=$chaveUnidades ?></li>
                            <ul>
                                <? foreach ($aptos as $chaveMorador => $morador) { ?>
                                    <? echo "<li>" ?>
                                    <ul><?=verificaUnidade($morador) ?></ul>
                                    <? } ?>
                                <? echo "</li>" ?>
                            </ul>
                        <? } ?>
                    </ul>
                <? } ?>
            </ul>
        <? } ?>
    </ul>
</body>
</html>