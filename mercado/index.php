<?php

error_reporting(0);

$frutas = array(
    "Maça",
    "Banana",
    "Pera",
    "Morango",
    "Goiaba",
    "Laranja",
    "Ameixa"
);

$carnes = array(
    "Frango",
    "Bisteca",
    "Picanha",
    "Coraçãozinho"
);

$padaria = array(
    "Bolos" => array(
        "Bolo de Cenoura",
        "Bolo de Chocolate",
        "Bolo de Limão",
        "Cuca"
    ),
    "Paes" => array(
        "Pão Francês",
        "Pão de Queijo",
        "Pão De Aipim",
        "Pão de Fubá"
    ),
    "Acompanhamentos" => array(
        "Café Preto",
        "Café com Leite",
        "Suco",
        "Chá",
        "Cookie com Cheddar"
    ),
    "Lanches" => array(
        "Coxinha",
        "Pastel de Carne",
        "Pastel de Frango",
        "Subway sem Molho"
    )
);

$mercearia = array(
    "Arroz",
    "Feijão",
    "Macarrão"
);

$mercado = array(
    "OrtiFruit" => $frutas,
    "Açougue" => $carnes,
    "Padaria" => $padaria,
    "Mercearia" => $mercearia
);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Mercado</h1>

    <ul>
        <? foreach($mercado as $keyCorredor => $corredor){?>
        <li><?=$keyCorredor?></li>
        <ul>
            <? foreach ($corredor as $keyPratileira => $valuePratileiras) { ?>
            <li><?=(is_array($valuePratileiras) ? $keyPratileira : $valuePratileiras) ?></li>
            
            <ul>
                <? foreach ($valuePratileiras as $keyBalcao => $valueBalcao) { ?>
                <li><?=$valueBalcao?></li>
                <? } ?>
            </ul>
            <? } ?>
        </ul>
        <? } ?>
    </ul>

</body>
</html>