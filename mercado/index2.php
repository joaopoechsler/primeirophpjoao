<?php

error_reporting(0);

$frutas = array(
    "Maça",
    "Banana",
    "Pera",
    "Morango",
    "Goiaba",
    "Laranja",
    "Ameixa"
);

$carnes = array(
    "Frango",
    "Bisteca",
    "Picanha",
    "Coraçãozinho"
);

$padaria = array(
    "Bolos" => array(
        "Bolo de Cenoura",
        "Bolo de Chocolate",
        "Bolo de Limão",
        "Cuca"
    ),
    "Paes" => array(
        "Pão Francês",
        "Pão de Queijo",
        "Pão De Aipim",
        "Pão de Fubá"
    ),
    "Acompanhamentos" => array(
        "Café Preto",
        "Café com Leite",
        "Suco",
        "Chá",
        "Cookie com Cheddar"
    ),
    "Lanches" => array(
        "Coxinha",
        "Pastel de Carne",
        "Pastel de Frango",
        "Subway sem Molho"
    )
);

$mercearia = array(
    "Arroz",
    "Feijão",
    "Macarrão"
);

$mercado = array(
    "OrtiFruit" => $frutas,
    "Açougue" => $carnes,
    "Padaria" => $padaria,
    "Mercearia" => $mercearia
);

echo "<h1>Mercado</h1>";

$estrutura = '';
foreach($mercado as $keyCorredor => $corredor){
    // echo "Corredor: ".$keyCorredor.'<br>';
 
     //laço do corredor
     foreach($corredor as $keyPratileira => $valuePratileiras){
 
         if(is_array($valuePratileiras)){
 
            // echo "&nbsp;&nbsp;&nbsp;&nbsp;Balcão: ".$keyPratileira.'<br>';
             //laço balcão
             foreach($valuePratileiras as $keyBalcao => $valueBalcao){
             //    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Produtos Balcão: ".$valueBalcao.'<br>';
             }
 
         }else{
            // echo "&nbsp;&nbsp;&nbsp;&nbsp;Pratileira: ".$valuePratileiras.'<br>';
         }
 
     }   
}
$estrutura .= '<ul>';
    foreach ($mercado as $keyCorredor => $corredor) {
    $estrutura .= "<li> $keyCorredor </li>";

    $estrutura .= '<ul>';
        foreach ($corredor as $keyPratileira => $valuePratileiras) {
        $estrutura .= "<li> $valuePratileiras </li>";

        $estrutura .= '<ul>';
            foreach($valuePratileiras as $keyBalcao => $valueBalcao){
            $estrutura .= "<li> </li>";
            }
        $estrutura .= '</ul>';
        }
    $estrutura .= '</ul>';
    }
$estrutura .= '</ul>';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <?=$estrutura?>    
</body>
</html>